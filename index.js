const express = require("express"),
ejs = require("ejs"),
mainRouter = require("./routes/mainRouter")(),
bodyParser = require("body-parser"),
// mongoose = require("mongoose"),
app = express();

// Views
app.set("view engine", "ejs");

// Middleware
app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// TODO: Database Connection

// Routers
app.use("/main", mainRouter);

// Home
app.get("/", (req, res) => {
    res.render("index", { userInfo: req.user, title: null });
  });
  
// Error
app.get("*", (req, res) => {
    res.status(404).render("error", {code: 404, message: "Not Found"});
});

app.use( (err, req, res, next) => {
    console.log(err)
    res.status(500).render("error", {code: 500, message: "Internal Server Error"});
  })

// Server
const server = app.listen(process.env.PORT || 3000, () => {
  console.log("listening for requests on port 3000");
});
  
  module.exports = server;