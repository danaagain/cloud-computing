const express = require("express"),
ejs = require("ejs");

const routes = () => {
    const mainRouter = express.Router();

    mainRouter.route("/").get(((req, res) => {
        res.render("main");
    }))

    return mainRouter;
};

module.exports = routes;